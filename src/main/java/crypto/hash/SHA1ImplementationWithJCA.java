package crypto.hash;

import org.apache.commons.codec.binary.Base32;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class SHA1ImplementationWithJCA {
    public static void main(String[] args) {
        String password = "p@ssw0rd" ;

        System.out.println("password: " + password);

        System.out.println("hash value:" + encryptPassword(password));
    }

    private static String encryptPassword(String password)
    {
        String sha1 = "";
        try
        {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
            Base32 base32 = new Base32();
            String encodedString = base32.encodeAsString(crypt.digest());
            System.out.println("Base 32 encoding: " + encodedString);


        }
        catch(NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch(UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        return sha1;
    }

    private static String byteToHex(final byte[] hash)
    {
        Formatter formatter = new Formatter();
        for (byte b : hash)
        {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
