package crypto.hash;

import org.bouncycastle.crypto.digests.KeccakDigest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.digests.SHA3Digest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

import java.security.Security;

public class SHAImplementation {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Security.addProvider(new BouncyCastleProvider());

        byte[] trial = "hello".getBytes();

        // TODO Auto-generated method stub

        //GOST3411Digest exampleSHA = new GOST3411Digest(); //256-bits
        //SHA1Digest exampleSHA = new SHA1Digest();
        //SHA256Digest exampleSHA = new SHA256Digest(); //256-bits
        //SHA384Digest exampleSHA = new SHA384Digest(); //384-bits
        //SHA512Digest exampleSHA = new SHA512Digest(); //512-bits
        //SHA3Digest exampleSHA = new SHA3Digest();
        SHA1Digest exampleSHA = new SHA1Digest();
        exampleSHA.update(trial, 0, trial.length);

        byte[] digested = new byte[exampleSHA.getDigestSize()];
        exampleSHA.doFinal(digested, 0);

        System.out.println("Input (hex): " + new String(Hex.encode(trial)));
        System.out.println("Output (hex): " + new String(Hex.encode(digested)));

    }

}

