package crypto.hash;

import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

public class MD5Implementation {
    

    /**
     * @param args
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        Security.addProvider(new BouncyCastleProvider());

        byte[] trial = "trial".getBytes();



        // TODO Auto-generated method stub
        MD5Digest exampleMD5 = new MD5Digest();
        exampleMD5.update(trial, 0, trial.length);

        byte[] digested = new byte[exampleMD5.getDigestSize()];
        exampleMD5.doFinal(digested, 0);

        System.out.println("Input (hex): " + new String(Hex.encode(trial)));
        System.out.println("Output (hex): " + new String(Hex.encode(digested)));


    }
}
