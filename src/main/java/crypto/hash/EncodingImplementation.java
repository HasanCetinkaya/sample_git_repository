package crypto.hash;


import org.apache.commons.codec.binary.Base32;

public class EncodingImplementation {
    public static void main(String... args) {
        String inputString = "Simple Solution";
        byte[] inputStringInBytes = inputString.getBytes();

        Base32 base32 = new Base32();
        String encodedString = base32.encodeAsString(inputStringInBytes);

        System.out.println("Input Data: " + inputString);
        System.out.println("Base32 Encoded Data: " + encodedString);
    }
}