package crypto.hash;

import org.bouncycastle.crypto.digests.RIPEMD320Digest;
import org.bouncycastle.crypto.digests.SHA3Digest;
import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.crypto.digests.TigerDigest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

import java.security.Security;
// this class can be used to generat prescription code
public class RIPEMD320Implementation {

    public static void main(String[] args) {
        Security.addProvider(new BouncyCastleProvider());

        byte[] trial = "hello".getBytes();

        // TODO Auto-generated method stub

        //GOST3411Digest exampleSHA = new GOST3411Digest(); //256-bits
        //SHA1Digest exampleSHA = new SHA1Digest();
        //SHA256Digest exampleSHA = new SHA256Digest(); //256-bits
        //SHA384Digest exampleSHA = new SHA384Digest(); //384-bits
        //SHA512Digest exampleSHA = new SHA512Digest(); //512-bits

        RIPEMD320Digest exampleSHA = new RIPEMD320Digest();
        //TigerDigest exampleSHA = new TigerDigest();

        exampleSHA.update(trial, 0, trial.length);

        byte[] digested = new byte[exampleSHA.getDigestSize()];
        exampleSHA.doFinal(digested, 0);

        System.out.println("Input (hex): " + new String(Hex.encode(trial)));
        System.out.println("Output (hex): " + new String(Hex.encode(digested)));
    }
}
